﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>", Scope = "member", Target = "~M:Clc.Polaris.Api.PapiClient.CreatePatronSystemBlockAsync(System.String,Clc.Polaris.Api.Models.SystemBlocks,System.Nullable{System.Int32},System.Nullable{System.Int32})~System.Threading.Tasks.Task{Clc.IRestResponse{Clc.Polaris.Api.Models.CreatePatronBlocksResult}}")]
[assembly: SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores", Justification = "<Pending>", Scope = "member", Target = "~M:Clc.Polaris.Api.IPapiClient.Synch_BibsByIdGetAsync(System.Int32)~System.Threading.Tasks.Task{Clc.IRestResponse{System.String}}")]
