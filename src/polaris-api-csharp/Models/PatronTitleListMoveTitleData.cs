﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Clc.Polaris.Api.Models
{
    public class PatronTitleListMoveTitleData
    {
        public int FromRecordStoreId { get; set; }
        public int FromPosition { get; set; }
        public int ToRecordStoreId { get; set; }
    }
}
