﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clc.Polaris.Api.Models
{
    public class PatronTitleListAddTitleData
    {
        public int RecordStoreId { get; set; }
        public int LocalControlNumber { get; set; }
    }
}
