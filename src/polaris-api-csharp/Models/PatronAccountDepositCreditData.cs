﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Clc.Polaris.Api.Models
{
    public class PatronAccountDepositCreditData
    {
        public double TxnAmount { get; set; }
        public string FreeTextNote { get; set; }
    }
}
