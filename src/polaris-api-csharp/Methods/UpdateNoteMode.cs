﻿namespace Clc.Polaris.Api
{
    public enum UpdateNoteMode
    {
        Replace,
        Prepend,
        Append
    }
}
